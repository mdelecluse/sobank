import json

from django.db import transaction
from django.core.serializers import serialize
from django.http import HttpResponse, HttpResponseBadRequest
from django.views import View

from .models import Customer, Account, transfer_money


class CustomerView(View):
    """
    Implementation of endpoints for customer-related operations :
    - List all customers
    - Create a new customer
    - Update a customer's information

    Methods allowed: GET, POST, PUT
    """

    @staticmethod
    def get(request, *args, **kwargs):
        """
        Get the list of all customers.

        @request.body: n/a.

        @response.body: list (as json).
            Contains dictionaries holding the customers' information; one dictionary per customer.
        """
        customers = Customer.objects.all()
        data = serialize("json", customers)  # Convert query to json
        return HttpResponse(data, content_type="application/json")

    @staticmethod
    def post(request, *args, **kwargs):
        """
        Create a new customer.

        @request.body: dict (as json).
            It must contain the following fields:
            - first_name:   str     max 50 chars
            - last_name:    str     max 50 chars
            - birth_date:   str     must follow the "YYYY-MM-DD" format
            - address:      str     max 200 chars

        @response.body: dict (as json).
            Contains the new customer's information.
        """
        data = json.loads(request.body)
        try:
            customer = Customer.make_from_json(data)
        except (TypeError, ValueError) as e:
            return HttpResponseBadRequest(reason=str(e))

        # Return the new customer data as response
        data = serialize("json", [customer])
        return HttpResponse(data, content_type="application/json")

    @staticmethod
    def put(request, *args, **kwargs):
        """
        Update an existing customer information.
        The customer is identified based on his 'id'.

        @request.body: dict (as json).
            It must contain the following fields:
            - id:           int     the ID of the customer whose information to update
            - first_name:   str     max 50 chars
            - last_name:    str     max 50 chars
            - birth_date:   str     must follow the "YYYY-MM-DD" format
            - address:      str     max 200 chars

        @response.body: dict (as json).
            Contains the updated customer's information.
        """
        data = json.loads(request.body)
        try:
            customer = Customer.update_from_json(data)
        except (TypeError, ValueError) as e:
            return HttpResponseBadRequest(reason=str(e))

        # Return the new customer data as response
        data = serialize("json", [customer])
        return HttpResponse(data, content_type="application/json")


class AccountView(View):
    """
    Implementation of endpoints for account-related operations :
    - List all accounts
    - Create a new account

    Methods allowed: GET, POST
    """

    @staticmethod
    def get(request, *args, **kwargs):
        """
        Get the list of all accounts.

        @request.body: n/a.

        @response.body: list (as json).
            Contains dictionaries holding the accounts' information; one dictionary per account.
        """
        accounts = Account.objects.all()
        data = serialize("json", accounts)  # Convert query to json
        return HttpResponse(data, content_type="application/json")

    @staticmethod
    def post(request, *args, **kwargs):
        """
        Create a new account.

        @request.body: dict (as json).
            It must contain the following fields:
            - owner_id:     int     the ID of the customer owning the account
            - type:         str     max 20 chars.
            - balance:      float   >= 0.0

        @response.body: dict (as json).
            Contains the new account's information.
        """
        data = json.loads(request.body)
        try:
            customer = Account.make_from_json(data)
        except (TypeError, ValueError) as e:
            return HttpResponseBadRequest(reason=str(e))

        # Return the new customer data as response
        data = serialize("json", [customer])
        return HttpResponse(data, content_type="application/json")


class TransferView(View):
    """
    Implementation of endpoints for Account-related operations :
    - Transfer money from an account to another

    Methods allowed: PUT
    """

    @staticmethod
    @transaction.atomic  # Ensure only 1 transaction at a time (cheap security)
    def put(request, *args, **kwargs):
        """
        Initiate a money transfer between two accounts.

        @request.body: dict (as json).
            It must contain the following fields:
            - account_origin:       int     the number of the account to transfer money from
            - account_destination:  int     the number of the account to transfer money to
            - amount:               float   >= 0.0 AND >= account_origin.balance

        @response.body: n/a
        """
        data = json.loads(request.body)
        try:
            transfer_money(data)
        except (TypeError, ValueError) as e:
            return HttpResponseBadRequest(reason=str(e))

        # Return the new customer data as response
        return HttpResponse()
