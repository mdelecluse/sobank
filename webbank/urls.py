from django.urls import path

from . import views

urlpatterns = [
    path('customer', views.CustomerView.as_view(), name='customer'),
    path('account', views.AccountView.as_view(), name='account'),
    path('transfer', views.TransferView.as_view(), name='transfer'),
]
