"""
Definition of helper functions used by models / views
"""
from geopy.exc import GeocoderServiceError
from geopy.geocoders import Nominatim


def geo_decode(address):
    """
    Decode an address into coordinates.

    Returns a (latitude, longitude) pair.
    In case the decoding fails, a (None, None) pair is returned instead.
    """
    # Use the 'OpenStreetMap Nominatim' free service.
    geo_locator = Nominatim(user_agent="webbank")
    try:
        location = geo_locator.geocode(address)
    except (PermissionError, GeocoderServiceError):
        location = None

    # Address could not be decoded -> return (None, None) as per spec.
    if location is None:
        return None, None
    return location.latitude, location.longitude
