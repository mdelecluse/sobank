from django.apps import AppConfig


class WebbankConfig(AppConfig):
    name = 'webbank'
